import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import CreateClothe from '@/components/CreateClothe'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Create',
      name: 'CreateClothes',
      component: CreateClothe
    },
    {
      path: '/',
      name: 'Home',
      component: Home
    }
  ]
})

